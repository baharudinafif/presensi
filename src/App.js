import "firebase/auth";
import _ from "lodash";
import firebase from "firebase";
import styled from 'styled-components';

import Form from './Component/Form';
import Rekap from './Component/Rekap';

import './App.css';

const firebaseConfig = {
  apiKey: "AIzaSyDeWsfJH06lTqnEuXRV62vUBT0Cc2ViXQI",
  authDomain: "solocorn.firebaseapp.com",
  projectId: "solocorn",
  storageBucket: "solocorn.appspot.com",
  messagingSenderId: "171163796779",
  appId: "1:171163796779:web:b3b217ecab9bdacd75b16e"
};

// eslint-disable-next-line no-unused-vars
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}

const Container = styled.div`
  display: grid;
  font-family: 'Roboto', sans-serif;
  grid-template-rows: 8em 1fr;
  font-size: 14pt;
  height: 100vh;
  
  > div.title {
    background: #2c3e50;
    text-align: center;
    padding: 2em 0;

    h5, h1 {
      color: white;
    }
    h5, h1 {
      padding: 0;
      margin: 0;
    }
    h5 { padding: .4em 0; }
    h1 { font-size: 2.6em; }
  }

  > div.content {
    display: grid;
    background: #1cbc9c;
    height: 100%;
    grid-template-rows: auto 1fr;

    hr {
      margin: 0.2em 0;
      border-width: 2px;
      border-style: solid;
      border-color: white;
    }
  }
`;

const App = () =>  {
  const pathname = _.get(window, 'location.pathname');
  if (pathname === '/rekap') { return <Rekap/> }
  return (<Container>
    <div className="title">
      <h5>Solo Technopark Co-working Space</h5>
      <h1>SOLOCORN <span style={{ color: "#1abc9c" }}>x</span> SMESKA</h1>
      <div>
        <img style={{ position: "absolute", height: "10em", top: "1.8em", left: "1em" }} src={process.env.PUBLIC_URL + '/logo.png'} alt="logo"/>
        <img style={{ position: "absolute", height: "10em", top: "1.6em", right: "0" }} src={process.env.PUBLIC_URL + '/stp.png'} alt="logo"/>
      </div>
    </div>
    <div className="content">
      <hr/>
      <div style={{ display: "grid" }}>
        <Form/>
      </div>
    </div>
  </Container>);
}

export default App;
