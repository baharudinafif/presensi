import _ from "lodash";
import styled from "styled-components";
import firebase from "firebase";
import { useState, useEffect } from "react";
import moment from "moment";

const Container = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr;
	grid-gap: 1.4em;
	padding: 1em; 
	// min-width: 21cm;
	// max-width: 21cm;
	// width: 21cm;
	// overflow: auto;
	
	font-family: "Roboto", sans-serif;

	p, h1, h2, h3 {
		margin: 0;
		padding: .2em 0 .4em;
	}
	font-size: 12pt;

	hr {
		margin: 0;
		padding: 0;
		border: 2px solid #666;
	}

	input { font-size: 1.2em; }
`

const firebaseConfig = {
  apiKey: "AIzaSyDeWsfJH06lTqnEuXRV62vUBT0Cc2ViXQI",
  authDomain: "solocorn.firebaseapp.com",
  projectId: "solocorn",
  storageBucket: "solocorn.appspot.com",
  messagingSenderId: "171163796779",
  appId: "1:171163796779:web:b3b217ecab9bdacd75b16e"
};

// eslint-disable-next-line no-unused-vars
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}

const retrieveData = async (db, coll) => new Promise((resolve) => {
	db.collection(coll).get().then((querySnapshot) => {
		const list = [];
		querySnapshot.forEach((doc) => {
			const val = doc.data();
			list.push(val);
		});
		return resolve(list);
	});
})


const ItemStyle = styled.div`
	display: grid;
	grid-template-columns: auto 1fr auto;
	align-items: center;
	border: 1px solid black;
	padding: 0.6em .8em; 
	grid-gap: .8em;
	border-radius: .4em;

	p { font-weight: 600; }
	
	img {
		border-radius: .4em;
		max-width: 6em;
	}
`;

const Item = ({ id, nama, email, noWA, tanggal, foto, tandaTangan }) => {
	const tgl = moment(tanggal).format('lll');
	return <ItemStyle key={id}> 
		<div>
			{!_.isNil(foto) && <img src={`https://firebasestorage.googleapis.com/v0/b/solocorn.appspot.com/o/${encodeURIComponent(foto)}?alt=media`} alt="foto"/>}
		</div>
		<div>
			<p>{nama}</p>
			<p>{email}</p>
			<p>{noWA}</p>
			<p>{tgl}</p>
		</div>
		<div>
			<p style={{ textAlign: 'center' }}>
				<img src={`https://firebasestorage.googleapis.com/v0/b/solocorn.appspot.com/o/${encodeURIComponent(tandaTangan)}?alt=media`} alt="ttd"/>
				<br/>
				<span style={{ fontSize: ".8em" }}>({nama})</span>
			</p> 
		</div>
	</ItemStyle>
}

const Rekap = () => {
	const [date, setDate] = useState();
	const [fetching, setFetching] = useState(false);
	const [list, setList] = useState([]);

	const onChange = (e) => {
		const value = _.get(e, 'target.value');
		setDate(value);
	}

	useEffect(() => {
		console.log('Date changed', date)
		const fetch = async () => {
			setFetching(true)
			const db = firebase.firestore();
			const coll = `presensi-${moment(date).format('YYMMDD')}`;
			const l = await retrieveData(db, coll);
			// console.log(l);
			setList(_.sortBy(l, 'tanggal'));
			setFetching(false)
		}
		
		if (!_.isNil(date)) { fetch(); }
	}, [date]);

	return <Container>
		<div style={{ gridColumn: 'span 2', textAlign: "center" }}>
			<h1>Rekapitulasi Presensi</h1>
			<h3> Tanggal: <input type="date" onChange={onChange}/></h3>

			<img style={{ position: "absolute", height: "6em", top: "1.8em", left: "1em" }} src={process.env.PUBLIC_URL + '/logo.png'} alt="logo"/>
			<img style={{ position: "absolute", height: "6em", top: "1.8em", right: "1em" }} src={process.env.PUBLIC_URL + '/stp.png'} alt="logo"/>

		</div>
		<div style={{ gridColumn: 'span 2', textAlign: "center" }}>
			<hr/>
		</div>
		{_.isEmpty(list) && <h3 style={{ gridColumn: "span 2", textAlign: "center", paddingTop: "4em", color: "#888"}}>
			{fetching ? 'Mengambil data...' : 'Data Presensi kosong'}
			</h3>}
		{list.map((o) => <Item key={o.id} {...o}/>)}
	</Container>
}

export default Rekap;