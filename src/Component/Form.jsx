import _ from "lodash";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Signature from "react-signature-canvas";
import firebase from "firebase";

import spinner from "../spinner.svg";
import success from "../success.svg";

const firebaseConfig = {
  apiKey: "AIzaSyDeWsfJH06lTqnEuXRV62vUBT0Cc2ViXQI",
  authDomain: "solocorn.firebaseapp.com",
  projectId: "solocorn",
  storageBucket: "solocorn.appspot.com",
  messagingSenderId: "171163796779",
  appId: "1:171163796779:web:b3b217ecab9bdacd75b16e"
};

// eslint-disable-next-line no-unused-vars
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}

const getBLOB = (canvas) => new Promise((resolve) => canvas.toBlob((o) => resolve(o)));
const uploadFile = (storageRef, fname, file) => new Promise((resolve) => {
	const fileRef = storageRef.child(fname);
	fileRef.put(file)
		.then((snapshot) => {
			console.log('Uploaded a File:', fname);
    	return resolve(snapshot);
		})
		.catch((err) => {
			console.log(err);
			return resolve();
		})
})
const retrieveData = async (db, coll) => new Promise((resolve) => {
	db.collection(coll).get().then((querySnapshot) => {
		const list = [];
		querySnapshot.forEach((doc) => {
			const val = doc.data();
			list.push(val);
		});
		return resolve(list);
	});
})

const Content = styled.div`
	width: 1200px;
	max-width: 1200px;
	justify-self: center;
	display: grid;
	grid-template-columns: 5fr 3fr;
	grid-template-rows: auto auto 1fr;
	grid-gap: 2em 2em;
	padding: 2em 0;

	label, span {
		position: relative;
    display: block;
    padding-top: .2em;
		font-weight: 700;
		letter-spacing: .6px;
	}

	p {
		color: #2c3e50;
		font-weight: 700;
		text-align: center;
		margin: 0; padding: 0;
	}

	#signature {
		grid-template-columns: 1fr;
		justify-items: center;
		grid-column: span 2;
		padding: 0;
	}
	> div:nth-child(2) {
		border-radius: 1.6em;
		padding: 2em;
		display: grid;
		position: relative;

		grid-template-columns: 5em 2em 1fr;
		transition: all 400ms;

		> div {
			margin-bottom: 1em;
			color: white;
		}

		.signature-pad {
			background: white;
			width: 40vw;
			height: 40vh;
			border-radius: 1em;
		}
	}
	> div:nth-child(3) {
		border-radius: 1.6em;
		display: grid;
		justify-items: right;
	}
	> div:nth-child(4) {
		grid-column: span 2;
		text-align: right;
	}
`

const Button = styled.button`
	font-size: 1.2em;
	border: none;
	box-shadow: none;
	padding: .5em 2em .4em 2em;
	border-radius: .5em;
	color: #aaa;
	letter-spacing: 1px;
	font-weight: 600;
	background: #2c3e50;

	&:hover {
		cursor: pointer;
		color: #fff;
	}
`

const Input = styled.input`
	background: none;
	box-shadow: none;
	border: none;
	border-bottom: 3px solid white;
	font-size: 1em;
	font-weight: 600;
	color: white;
	padding: .2em .2em .4em .2em;
	letter-spacing: 1px;
	width: 100%;

	&:focus-visible {
		border: none;
		box-shadow: none;
		outline: none;
		color: #1abc9c;
		border-bottom: 3px solid #1abc9c;
	}

	::placeholder {
		color: rgba(200,200,200, 0.5);
	}
`

const Video = styled.video`
	width: 100%;
	border-radius: 1.6em;
	transform: rotateY(180deg);
`
const Canvas = styled.canvas`
	width: 80%;
	border-radius: 1.6em;
	transform: rotateY(180deg);
`

const Form = () => {
	const [timerId, setTimerId] = useState();
	const [stream, setStream] = useState();
	const [time, setTime] = useState(moment());
	const [formData, setFormData] = useState({});
	const [signature, setSignature] = useState();
	const [sending, setSending] = useState(0);
	const [step, setStep] = useState(0);
	const [mapUser, setMapUser] = useState({});

	const onChange = (e) => {
		const name = _.get(e, 'target.name', '');
		const value = _.toUpper(_.get(e, 'target.value', ''));
		const data = { ...formData, [name]: value };

		if (name === 'email') {
			const user = _.chain(mapUser).get(value, {}).value();
			_.set(data, 'nama', _.get(user, 'nama', ''));
			_.set(data, 'noWA', _.get(user, 'noWA', ''));
		}
		setFormData(data);
	}
	const onClick = async (e) => {
		const name = _.get(e, 'target.name');

		if (name === 'lanjut') {
			const video = document.querySelector('video');
			const canvas = window.canvas = document.getElementById('canvas-foto');

			canvas.width = video.videoWidth;
			canvas.height = video.videoHeight;
			canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
			const blob = await getBLOB(canvas);
			setFormData({ ...formData, foto: blob });

			try {
				stream.getTracks().forEach((track) => track.stop());
			} catch (error) {
				console.log(error);
			} finally {
				setStep(step + 1);
			}
		} else if (name === 'submit') {
			setStep(2);
			setSending(1);

			const id = uuidv4();
			const db = firebase.firestore();
			const coll = `presensi-${moment().format('YYMMDD')}`;

			const canvas = window.canvas = document.getElementById('canvas-sign');
			const foto = _.get(formData, 'foto');
			const sign = await getBLOB(canvas);

			const storageRef = firebase.storage().ref();
			const fnameSign = `${coll}/${id}#sign.jpg`;
			const fnameFoto = `${coll}/${id}#foto.jpg`;

			const doc = {
				id,
				nama: _.get(formData, 'nama'),
				email: _.get(formData, 'email'),
				noWA: _.get(formData, 'noWA'),
				foto: _.isNil(foto) ? null : fnameFoto,
				tandaTangan: fnameSign,
				tanggal: (new Date()).toISOString(),
			}
			const user = {
				nama: _.chain(doc).get('nama').toString().trim().value(),
				noWA: _.chain(doc).get('noWA').toString().trim().value(),
				email: _.chain(doc).get('email').toString().trim().value(),
			};

			await Promise.all([
				!_.isNil(foto) && uploadFile(storageRef, fnameFoto, foto),
				uploadFile(storageRef, fnameSign, sign),
				db.collection(coll).doc(doc.id).set(doc),
				db.collection('users').doc(user.email).set(user),
			]);
			console.log('Saving Complete');

			// const list = await Promise.all([retrieveData(db, coll), retrieveData(db, 'users')]);
			// console.log(list[0], list[1]);
				
			setSending(2);
			setTimeout(() => { window.location.reload(); }, 1000);
		} else if (name === 'clear') {
			try {
				signature.clear();
			} catch (error) {
				console.log('CLEAR', error);
			}
		}
	}

	const nama = _.get(formData, 'nama', '');
	const noWA = _.get(formData, 'noWA', '');
	const foto = _.get(formData, 'foto', null);

	const focus = () => {
		const elm = document.getElementById('input-email');
		elm && elm.focus();
	}

	useEffect(() => {
		const timer = setInterval(() => {
			setTime(moment());
		}, 1000);
		setTimerId(timer);

		focus();
	}, []);

	useEffect(() => {
		return () => { clearInterval(timerId); }
	}, [timerId])

	useEffect(() => {
		const getStream = async () => {
			const str = await navigator.mediaDevices.getUserMedia({ video: { width: 500, height: 500 } })
			setStream(str);
		}
		const fetchData = async () => {
			const db = firebase.firestore();
			const list = await retrieveData(db, 'users');
			const map = _.keyBy(list, 'email'); 
			setMapUser(map);
			// console.log({ map })
		}
		if (step === 0) {
			getStream();
			fetchData();
		}
	}, [step])

	useEffect(() => {
		const video = document.querySelector('video');
		if (!_.isEmpty(video)) {
			video.srcObject = stream;
			video.onloadedmetadata = () => { video.play() }
		}
	}, [stream]);

	return <Content>
		<div style={{ gridColumnEnd: "span 2", paddingBottom: "1em" }}>
			{step !== 2 && <p>Halo, mari absen dulu yuk!</p>}
		</div>
		{step === 0 && <>
			<div style={{ background: "#3333339e" }}>
				<div><label>Email</label></div>
				<div><span>:</span></div>
				<div><Input autoComplete="off" id="input-email" name="email" defaultValue="" placeholder="Alamat surat elektronik Anda" onChange={onChange}/></div>

				<div><label>Nama</label></div>
				<div><span>:</span></div>
				<div><Input autoComplete="off" name="nama" value={nama} placeholder="Nama Anda" onChange={onChange}/></div>
				
				<div><label>No WA</label></div>
				<div><span>:</span></div>
				<div><Input autoComplete="off" name="noWA" value={noWA} placeholder="No Whatsapp Anda" onChange={onChange}/></div>
								
				<div><label>Waktu</label></div>
				<div><span>:</span></div>
				<div><span>{time.format('lll')}</span></div>	
			</div>
			<div style={{ display: 'grid' }}>
				<Video hidden={!_.isNil(foto)}/>
				<Canvas id="canvas-foto" hidden={_.isNil(foto)}/>
			</div>
			<div style={{ paddingTop: "1em" }}>
				<Button name="lanjut" onClick={onClick}>LANJUT</Button>
			</div>
		</>}

		{step === 1 && <>
			<div id="signature">
				<p style={{ paddingBottom: '.4em'}}>Tanda Tangan</p>
				<Signature penColor='black' ref={(ref) => setSignature(ref)} canvasProps={{ id: "canvas-sign", className: "signature-pad" }}/>
				<i style={{ paddingTop: '.4em' }}>masukkan tanda tangan Anda pada area berwarna putih</i>
			</div>
			<div style={{ gridColumn: "span 2", paddingTop: "1em", justifyContent: "center" }}>
				<p>
					<Button style={{ background: "#127863" }} name="clear" onClick={onClick}>BERSIHKAN</Button> &nbsp; &nbsp;
					<Button name="submit" onClick={onClick}>KIRIM</Button>
				</p>
			</div>
		</>}

		{step === 2 && <div style={{ gridColumn: "span 2", justifySelf: "center" }}>
			{sending === 1 && <div style={{ gridColumn: "span 3" }}>
				<div style={{ textAlign: "center" }}><img style={{ height: "8em" }} src={spinner} alt="spinner"/></div>
				<p style={{ fontWeight: "400", fontSize: "1em" }}>Mengirim presensi...</p>
			</div>}
			
			{sending === 2 && <div style={{ gridColumn: "span 3" }}>
				<div style={{ textAlign: "center" }}><img style={{ height: "8em" }} src={success} alt="success"/></div>
				<p style={{ fontWeight: "400", fontSize: "1em" }}>
					Presensi terkirim, Terima kasih :)
				</p>
			</div>}			
		</div>}
	</Content>
}

export default Form;